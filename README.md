# Vibsnd

## Overview
**Vibsnd** is a machine learning project that uses deep learning to separate
two simultaneously occurred impact sounds, which are ubiquitous in any two-body
collision. At inference time, the input is a mixed sound waveform and
vibrational data such as those gathered from accelerometers on *one* of the two
objects.
