#!/usr/bin/env python
import numpy as np
import struct,subprocess,pickle,random
################################################################################
class Dataset:
    def __init__(self):
        self.N_examples = 0
        self.example_files = []
        self.example_pairs = []
        self.file2id = dict()
        self.id2file = dict()

    @staticmethod
    def EncodeKey(f1_snd, f2_snd, f1_vib):
        return f1_snd + ':::' + f2_snd + ':::' + f1_vib

    @staticmethod
    def DecodeKey(encoded_name):
        f1_snd, f2_snd, f1_vib = encoded_name.strip().split(':::')
        return f1_snd, f2_snd, f1_vib

    def ReadFileIdMapping(self, file2id, id2file):
        self.file2id = dict()
        self.id2file = dict()
        with open(file2id, 'r') as stream:
            lines = stream.readlines()
            for line in lines:
                key, val = line.split()
                val = int(val)
                self.file2id[key] = val
        with open(id2file, 'r') as stream:
            lines = stream.readlines()
            for line in lines:
                key, val = line.split()
                key = int(key)
                self.id2file[key] = val

    def Save(filename):
        pickle.dump(self, open(filename, 'wb'))

    def Load(filename):
        pickle.load(open(filename, 'rb'))

    @staticmethod
    def TrainValTestSplit(filenames, val_ratio=0.1, test_ratio=0.1,
                          shuffle=True):
        train_ratio = 1.0 - val_ratio - test_ratio
        assert train_ratio > 0.0 and train_ratio <= 1.0
        random.shuffle(filenames)
        length = len(filenames)
        len_val  = int(np.floor(val_ratio * length))
        len_test = int(np.floor(test_ratio * length))
        len_train= length - len_val - len_test
        train_set = filenames[:len_train]
        val_set = filenames[len_train:len_train+len_val]
        test_set = filenames[-len_test:]
        return train_set, val_set, test_set
################################################################################
def RunCmd(cmd, exe=True):
    print(cmd)
    if exe:
        subprocess.call(cmd, shell=True)
################################################################################
def ReadRawData(filename):
    with open(filename, 'rb') as stream:
        N = struct.unpack('i', stream.read(4))[0]
        data = np.array(struct.unpack('%ud' %(N), stream.read(N*8)))
        return data
    return None
################################################################################
def GetAmp(data):
    return np.sqrt(np.power(data[:,:,0], 2) + np.power(data[:,:,1], 2))
################################################################################
