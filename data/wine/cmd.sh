python ~/code/gpu_wavesolver/src/python/generate_random_impulses.py /hdd2/tpat/data/models/wine/wine.tet.obj 100 0 > impulse.txt
python -u ../gen_single_sound.py /hdd2/tpat/data/models/wine/wine.tet.obj /hdd2/tpat/data/models/wine/modal_models/glass/wine_surf.modes ~/data/models/materials/glass.txt /hdd2/tpat/data/models/wine/radiation_models/glass_M1K3/ffat_map-fdtd impulse.txt
