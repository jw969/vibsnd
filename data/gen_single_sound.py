#!/usr/bin/env python
import subprocess,struct,sys,os
import scipy.io.wavfile
from data_utils import ReadRawData
import numpy as np

def RunCmd(cmd, exe=True):
    print cmd
    subprocess.call(cmd, shell=True)

if len(sys.argv) != 6:
    print '**Usage: %s <obj> <mode> <material> <ffat_map_folder> <impulse_file>' %(sys.argv[0])
    sys.exit()

BIN = '/home/jui-hsien/code/gpu_wavesolver/build/modal_simulator_bin'
obj = sys.argv[1]
mod = sys.argv[2]
mat = sys.argv[3]
fat = sys.argv[4]
imp = sys.argv[5]
T = 1.0
listen = [1., 1., 1.]
samp_rate = 48000
randomize_listen = True
N_set = None
# N_set = range(10)
outdir = '.'
surfacc_vtx = 0 # TODO maybe use some other values?
RunCmd('mkdir -p %s' %(outdir))

with open(imp, 'r') as stream:
    lines = stream.readlines()
    N = len(lines)

if N_set is not None:
    N = len(N_set)

for ii_ in range(N):
    if N_set is not None:
        ii = N_set[ii_]
    else:
        ii = ii_
    with open('impulse_tmp.txt', 'w') as stream:
        tokens = lines[ii].split()[1:]
        tokens.insert(0, '0.0')
        stream.write(' '.join(tokens))
    if randomize_listen:
        r = 0.3
        theta = np.random.uniform(0, 2*np.pi)
        phi = np.random.uniform(0, 2*np.pi)
        listen[0] = r*np.sin(theta)*np.cos(phi)
        listen[1] = r*np.sin(theta)*np.sin(phi)
        listen[2] = r*np.cos(theta)
    print 'listen = ', listen
    RunCmd('%s -m %s -s %s -t %s -i %s -l %f -fatcube %s -listen %f %f %f -o %s/output_sound-%u.dat --outfile_surfacc %s/output_acc-%u.dat --vtx_surfacc %u -f %u'
          % (BIN, obj, mod, mat, 'impulse_tmp.txt', T, fat, listen[0], listen[1], listen[2], outdir, ii, outdir, ii, surfacc_vtx, samp_rate))
    data = ReadRawData('%s/output_sound-%u.dat'%(outdir, ii))
    data_acc = ReadRawData('%s/output_acc-%u.dat'%(outdir, ii))
    scipy.io.wavfile.write('%s/output_sound-%u.wav' %(outdir, ii), 44100.,
                           np.asarray(np.array(data)/max(abs(data))*32767, dtype=np.int16))
    scipy.io.wavfile.write('%s/output_acc-%u.wav' %(outdir, ii), 44100.,
                           np.asarray(np.array(data_acc)/max(abs(data_acc))*32767, dtype=np.int16))
