#!/usr/bin/env python
import os,struct,glob,sys
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from data_utils import ReadRawData, GetAmp, RunCmd, Dataset
sys.path.append('../model/lib')
from utils import fast_stft, fast_istft, fast_cRM, fast_icRM, generate_cRM, stft, istft
################################################################################
def InitDirs(base_path):
    try:
        os.makedirs('%s/examples' %(base_path))
        os.makedirs('%s/meta' %(base_path))
    except OSError:
        pass
################################################################################
def ResampleData(data, input_freq=44100, output_freq=16000):
    N_t = int(data.shape[0]/input_freq)
    data = signal.resample(data, N_t*output_freq)
    return data
################################################################################
def GenOneExample(snd1, snd2, vib1):
    mix  = snd1 + snd2
    snd1 = fast_stft(snd1)
    snd2 = fast_stft(snd2)
    vib1 = fast_stft(vib1)
    mix  = fast_stft(mix)

    crm1 = fast_cRM(snd1, mix)
    crm2 = fast_cRM(snd2, mix)
    assert crm1.shape == crm2.shape
    assert vib1.shape == mix.shape
    Y = np.empty((*crm1.shape, 2))
    X = np.empty((*vib1.shape, 2))
    Y[:,:,:,0] = crm1
    Y[:,:,:,1] = crm2
    X[:,:,:,0] = mix
    X[:,:,:,1] = vib1
    return X, Y
################################################################################
def GenOneExampleFromFile(f1_snd, f2_snd, f1_vib):
    # side
    print(f1_snd, f2_snd, f1_vib)
    snd1 = ReadRawData(f1_snd)
    snd2 = ReadRawData(f2_snd)
    vib1 = ReadRawData(f1_vib)
    return GenOneExample(snd1, snd2, vib1)
################################################################################
def GenAllExamples(objs, dataset_base, save_entire_db=False, overwrite=False):
    InitDirs(dataset_base)
    dataset = Dataset()
    path = '%s/meta/filenames.txt' %(dataset_base)
    filenames = []
    if not os.path.isfile(path) or overwrite:
        for obj_1 in objs:
            f1_sndfiles = glob.glob('%s/output_sound-*.dat' %(obj_1))
            for obj_2 in objs:
                if obj_2 == obj_1:
                    continue
                f2_sndfiles = glob.glob('%s/output_sound-*.dat' %(obj_2))
                for f1_snd in f1_sndfiles:
                    f1_vib = f1_snd.replace('_sound', '_acc')
                    for f2_snd in f2_sndfiles:
                        x, y = GenOneExampleFromFile(f1_snd, f2_snd, f1_vib)
                        example_key = Dataset.EncodeKey(f1_snd, f2_snd, f1_vib)
                        dataset.example_files.append(example_key)
                        if save_entire_db: # exhaust memory so only when needed
                            dataset.example_pairs.append((x, y))
                        dataset.file2id[example_key] = dataset.N_examples
                        dataset.id2file[dataset.N_examples] = example_key
                        filename = '%s/examples/example_%u.npy' %(
                            dataset_base, dataset.N_examples)
                        np.save(filename, (x,y))
                        filenames.append(filename)
                        dataset.N_examples += 1
        with open(path, 'w') as stream:
            for filename in filenames:
                stream.write('%s\n' %(filename))
        with open('%s/meta/id2file.txt' %(dataset_base), 'w') as stream:
            for key in dataset.id2file:
                stream.write('%u %s\n' %(key, dataset.id2file[key]))
        with open('%s/meta/file2id.txt' %(dataset_base), 'w') as stream:
            for key in dataset.file2id:
                stream.write('%s %u\n' %(key, dataset.file2id[key]))
        if save_entire_db:
            dataset.Save('%s/dataset.pkl' %(dataset_base))
    else:
        with open(path, 'r') as stream:
            filenames = stream.readlines()
            filenames = [x.split()[0] for x in filenames]
        print('**NOTE** dataset exists and not overwriting; \
              read filenames from disk')
    return filenames
################################################################################
if __name__ == '__main__':
    objs = [
        'bowl',
        'bunny',
        'plate',
        'wine'
    ]
    dataset_base = '/home/jui-hsien/code/vibsnd/data/dataset'
    filenames = GenAllExamples(objs, dataset_base, overwrite=False)
    train_files, val_files, test_files = Dataset.TrainValTestSplit(
        filenames, 0.1, 0.1, True)
    print('train =', train_files)
    print('val =', val_files)
    print('test =', test_files)
