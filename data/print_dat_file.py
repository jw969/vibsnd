#!/usr/bin/env python
import sys
import numpy as np
import matplotlib.pyplot as plt
from data_utils import ReadRawData
################################################################################
if __name__ == '__main__':
    plt.figure()
    mix = None
    plt.subplot(2,1,1)
    for f in sys.argv[1:]:
        data = ReadRawData(f)
        if mix is None:
            mix = data.copy()
        else:
            mix += data.copy()
        print('file:', f)
        print(data)
        print(min(data), max(data))
        plt.plot(data, label=f)
        plt.xlim([0,20000])
        plt.legend()
    plt.subplot(2,1,2)
    print('mix:')
    print(mix)
    print(min(mix), max(mix))
    plt.plot(mix, label='mix')
    plt.xlim([0,20000])
    plt.legend()
    plt.show()
