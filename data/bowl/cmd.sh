python ~/code/gpu_wavesolver/src/python/generate_random_impulses.py /hdd2/tpat/data/models/bowl/bowl.tet.obj 100 0 > impulse.txt
python -u ../gen_single_sound.py /hdd2/tpat/data/models/bowl/bowl.tet.obj /hdd2/tpat/data/models/bowl/modal_models/wood/bowl_surf.modes ~/data/models/materials/wood.txt /hdd2/tpat/data/models/bowl/radiation_models/wood_M1K3/ffat_map-fdtd impulse.txt
