python ~/code/gpu_wavesolver/src/python/generate_random_impulses.py /hdd2/tpat/data/models/plate/plate.tet.obj 100 0 > impulse.txt
python -u ../gen_single_sound.py /hdd2/tpat/data/models/plate/plate.tet.obj /hdd2/tpat/data/models/plate/modal_models/ceramics/plate_surf.modes ~/data/models/materials/ceramics.txt /hdd2/tpat/data/models/plate/radiation_models/ceramics_M1K3/ffat_map-fdtd impulse.txt
