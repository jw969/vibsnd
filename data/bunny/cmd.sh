python ~/code/gpu_wavesolver/src/python/generate_random_impulses.py /hdd2/tpat/data/models/bunny/bunny.tet.obj 100 0 > impulse.txt
python -u ../gen_single_sound.py /hdd2/tpat/data/models/bunny/bunny.tet.obj /hdd2/tpat/data/models/bunny/modal_models/abs/bunny_surf.modes ~/data/models/materials/abs.txt /hdd2/tpat/data/models/bunny/radiation_models/abs_M1K3/ffat_map-fdtd impulse.txt
