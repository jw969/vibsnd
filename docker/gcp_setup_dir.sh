#!/bin/bash
# mount the dataset disk

mount=/mnt/disks/sdb
dataset_base=${mount}/research_data/vibsnd_dataset/dataset2
out_base=${HOME}/data/vibsnd_dataset/training_instance

sudo mkdir -p ${mount}
sudo mount -o discard,defaults /dev/sdb ${mount}
sudo chmod a+r ${mount}

# setup out directory
mkdir -p ${out_base}

# pull from docker
nvidia-docker pull jhwang7628/vibsnd

# docker runtime mount command
echo -e "\nStep 1: Start the docker container with the right mount"
echo "nvidia-docker run -it --rm -v ${dataset_base}:${dataset_base} -v ${out_base}:${out_base} jhwang7628/vibsnd bash"
echo -e "\nStep 2: Start the training"
echo "cd /vibsnd/model; python3 train.py ${out_base} ${dataset_base}"
