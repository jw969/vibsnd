#!/usr/bin/env python
from keras.models import load_model
from keras import optimizers
import scipy.io.wavfile as wavfile
import numpy as np
import sys,subprocess,glob
sys.path.append('lib')
sys.path.append('../data')
from data_utils import Dataset
import utils
from example_generator import VibroSoundGenerator
################################################################################
def RunCmd(cmd, exe=True):
    print(cmd)
    if exe:
        subprocess.call(cmd, shell=True)
################################################################################
def F2T(signal_f, normalize=True):
    signal_t = utils.fast_istft(signal_f, power=False)
    signal_t /= max(abs(signal_t))
    return signal_t
################################################################################
def BestModel(model_base):
    GetLoss = lambda x : float('.'.join(x.split('/')[-1].split('-')[-1].split('.')[0:2]))
    filenames = sorted(glob.glob(model_base + '/*.h5'), key=GetLoss)
    print(filenames)
    return filenames[0]

################################################################################
if __name__ == '__main__':

    # model_base = '/hdd1/research_data/vibsnd_dataset/training_instance/instance-1/16/saved_models'
    # out_base = 'tests_instance-1'
    # model_base = '/hdd1/research_data/vibsnd_dataset/training_instance/dl-instance-2/3/saved_models'
    # out_base = 'tests_dl-instance-2'
    model_base = '/hdd1/research_data/vibsnd_dataset/training_instance/dl-instance-3/10/saved_models'
    out_base = 'tests_dl-instance-3'
    model_path = BestModel(model_base)
    dataset_base = '/hdd1/research_data/vibsnd_dataset/dataset2'
    RunCmd('mkdir -p %s' %(out_base))

    # model_path = '%s/model-2p-006-0.0119238039.h5' %(model_base)
    print('Using model', model_path)
    test_path = '%s/test_files.txt' %(model_base)

    # need to compile the model for multi-gpu setup
    model = load_model(model_path)
    model.compile(optimizer=optimizers.Adam(),
                  loss='mean_squared_error',
                  metrics=['accuracy'])

    # read key/val mapping for filenames
    dataset = Dataset()
    dataset.ReadFileIdMapping('%s/meta/file2id.txt' %(dataset_base),
                              '%s/meta/id2file.txt' %(dataset_base))
    GetFileId = lambda x: int(x.split('_')[-1].split('.')[0])

    with open(test_path, 'r') as stream:
        filenames = stream.readlines()
        filenames = [x[:-1] for x in filenames]
        generator = VibroSoundGenerator(dataset_base, filenames, batch_size=1, shuffle=False)
        for ii in range(len(generator)):
            # predict
            X, Y = generator[ii]
            mix, vib1 = X
            mix = mix[0,:,:,:]
            vib1 = vib1[0,:,:,:]
            Y_predict = model.predict(X)
            crm1 = Y[0,:,:,:,0]
            crm2 = Y[0,:,:,:,1]
            crm1_predict = Y_predict[0,:,:,:,0]
            crm2_predict = Y_predict[0,:,:,:,1]
            clean1 = utils.fast_icRM(mix, crm1)
            clean2 = utils.fast_icRM(mix, crm2)
            clean1_predict = utils.fast_icRM(mix, crm1_predict)
            clean2_predict = utils.fast_icRM(mix, crm2_predict)
            clean1_t = F2T(clean1)
            clean2_t = F2T(clean2)
            clean1_t_predict = F2T(clean1_predict)
            clean2_t_predict = F2T(clean2_predict)
            vib1_t = F2T(vib1)
            mix_t = F2T(mix)

            # write
            outdir = '%s/%u' %(out_base, ii)
            RunCmd('mkdir -p %s' %(outdir))
            loss = model.evaluate(X, Y, batch_size=1)
            f1snd, f2snd, f1vib = dataset.DecodeKey(
                dataset.id2file[GetFileId(filenames[ii])])
            with open('%s/filename.txt' %(outdir), 'w') as stream:
                stream.write(filenames[ii] + '\n')
                stream.write(f1snd + '\n')
                stream.write(f2snd + '\n')
                stream.write(f1vib + '\n')
            print(ii, 'loss = {}\n'.format(loss), file=open('%s/loss.txt' %(outdir), 'w'))
            wavfile.write('%s/in_mix.wav' %(outdir), 48000, mix_t)
            wavfile.write('%s/in_vib1.wav' %(outdir), 48000, vib1_t)
            wavfile.write('%s/1_truth.wav' %(outdir), 48000, clean1_t)
            wavfile.write('%s/2_truth.wav' %(outdir), 48000, clean2_t)
            wavfile.write('%s/1_predict.wav' %(outdir), 48000, clean1_t_predict)
            wavfile.write('%s/2_predict.wav' %(outdir), 48000, clean2_t_predict)
