#/usr/bin/env python
import os,sys,subprocess,glob
import numpy as np
import tensorflow as tf
from keras import optimizers
from keras.models import load_model
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras.callbacks import TensorBoard
import keras.callbacks
sys.path.append('../data')
sys.path.append('./lib')
from example_generator import VibroSoundGenerator
from data_utils import Dataset
from model_ops import LatestFile
from model_deeper import NNModel,NNModelGpu
################################################################################
def RunCmd(cmd, exe=True):
    print(cmd)
    if exe:
        subprocess.call(cmd, shell=True)
################################################################################
class LogManagementCallback(keras.callbacks.Callback):
    def __init__(self, work_dir='.', store_dir='.'):
        self.work_dir = work_dir
        self.store_dir = store_dir
    def on_train_begin(self, logs={}):
        self.losses = []
        self.acc = []
    def on_batch_end(self, batch, logs={}):
        # get loss and acc and print
        loss = logs.get('loss')
        acc = logs.get('acc')
        self.losses.append(logs)
        self.acc.append(acc)
        print('\nBatch {}:\nloss={}\nacc={}\n'.format(batch, loss, acc))
################################################################################
def scheduler(epoch):
    '''
    Learning rate scheduler
    '''
    ini_lr = 0.001
    lr = ini_lr
    if epoch >= 5:
        lr = ini_lr / 5
    if epoch >= 10:
        lr = ini_lr / 10
    if epoch >= 30:
        lr = ini_lr / 20
    return lr
################################################################################
def WriteToFile(train_files, filename):
    with open(filename, 'w') as stream:
        for f in train_files:
            stream.write('%s\n' %(f))
################################################################################
def ReadData(dataset_base):
    with open('%s/meta/filenames.txt' %(dataset_base), 'r') as stream:
        filenames = stream.readlines()
        filenames = [x.strip('\n') for x in filenames]
    train_files, val_files, test_files = Dataset.TrainValTestSplit(
        filenames, 0.05, 0.05, True)
    print('train set size', len(train_files))
    print('val set size', len(val_files))
    print('test set size', len(test_files))
    return train_files, val_files, test_files
################################################################################
def GetAvailFolderID(path):
    FileID = lambda x: int(x.split('/')[-1])
    ids = sorted(glob.glob(path+'/*'), key=FileID)
    if len(ids) == 0:
        return 0
    return FileID(ids[-1])+1
################################################################################
if __name__ == '__main__':
    ###
    # configs and set up filesystems
    resume = False
    if len(sys.argv) >= 3:
        out_base = sys.argv[1]
        dataset_base = sys.argv[2]
    else:
        out_base = '/hdd1/research_data/vibsnd_dataset/training_instance'
        dataset_base = '/home/jui-hsien/code/vibsnd/data/dataset2'
    training_idx = GetAvailFolderID(out_base)
    outdir = out_base + '/%u' %(training_idx)
    path = '%s/saved_models' %(outdir)
    use_gpus = 0
    RunCmd('mkdir -p %s/logs %s/saved_models ./training_instance'
           %(outdir, outdir))
    RunCmd('ln -s %s ./training_instance/%u' %(outdir, training_idx))
    # hyperparameters
    epochs = 50
    initial_epoch = 0
    learning_rate_callback = LearningRateScheduler(scheduler, verbose=1)
    multi_gpus = False
    if not multi_gpus:
        num_gpus = 1
        batch_size = 4 # on local Titan X
    else:
        num_gpus = 4
        batch_size = num_gpus * 10 # on Tesla K80
    ###
    # prepare examples using generators
    train_files, val_files, test_files = ReadData(dataset_base)
    WriteToFile(train_files, path+'/train_files.txt')
    WriteToFile(test_files, path+'/test_files.txt')
    WriteToFile(val_files, path+'/val_files.txt')
    train_generator = VibroSoundGenerator(
        dataset_base,
        train_files,
        batch_size=batch_size,
        shuffle=True)
    val_generator = VibroSoundGenerator(
        dataset_base,
        val_files,
        batch_size=batch_size,
        shuffle=True)
    # configure callbacks
    folder = os.path.exists(path)
    if not folder:
        os.makedirs(path)
        print('create folder to save models')
    filepath = path + "/model-2p-{epoch:03d}-{val_loss:.10f}.h5"
    checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1,
                                 save_best_only=False, mode='min')
    sys.stdout = open('%s/train.log' %(outdir), 'a')
    sys.stderr = open('%s/train.log' %(outdir), 'a')
    callback_list = [TensorBoard(log_dir='%s/logs' %(outdir)),
                     checkpoint,
                     learning_rate_callback]
    # build the model
    nn_model = None
    opt = None
    if resume:
        last_file = LatestFile(path)
        nn_model = load_model(last_file)
        info = last_file.strip().split('-')
        initial_epoch = int(info[-2])
        print('Resuming the model fitting from epoch', initial_epoch)
    else:
        nn_model = NNModel()
        opt = optimizers.Adam()
    print(nn_model.summary())

    if not multi_gpus:
        nn_model.compile(optimizer=opt, loss='mean_squared_error',
                         metrics=['accuracy'])
        nn_model.fit_generator(generator=train_generator,
                               validation_data=val_generator,
                               epochs=epochs,
                               callbacks=callback_list,
                               initial_epoch=initial_epoch
                               )
    else:
        nn_model_parallel = NNModelGpu(nn_model, num_gpus)
        nn_model_parallel.compile(optimizer=opt, loss='mean_squared_error',
                                  metrics=['accuracy'])
        nn_model_parallel.fit_generator(generator=train_generator,
                                        validation_data=val_generator,
                                        epochs=epochs,
                                        callbacks=callback_list,
                                        initial_epoch=initial_epoch
                                       )
