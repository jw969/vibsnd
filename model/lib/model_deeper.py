import os,sys
import numpy as np
import tensorflow as tf
from keras.models import Sequential
from keras.layers import Input, Dense, Convolution2D, Bidirectional, concatenate
from keras.layers import Flatten, BatchNormalization, ReLU, Reshape, TimeDistributed
from keras.layers.recurrent import LSTM
from keras.initializers import he_normal, glorot_uniform
from keras.models import Model
from keras.utils import multi_gpu_model
################################################################################
class NNModelGpu(Model):
    def __init__(self, serial_model, num_gpus):
        parallel_model = multi_gpu_model(serial_model, num_gpus)
        self.__dict__.update(parallel_model.__dict__)
        self._smodel = serial_model

    def __getattribute__(self, attrname):
        '''
        Override load and save methods to be used from the serial-model. The
        serial-model holds references to the weights in the multi-gpu model.
        '''
        # return Model.__getattribute__(self, attrname)
        if 'load' in attrname or 'save' in attrname:
            return getattr(self._smodel, attrname)

        return super(NNModelGpu, self).__getattribute__(attrname)
################################################################################
def NNModel():
    ### Audio subnetwork
    audio_input = Input(shape=(298, 257, 2))
    print('as_0:', audio_input.shape)
    as_conv1 = Convolution2D(48, kernel_size=(1, 7), strides=(1, 1), padding='same', dilation_rate=(1, 1), name='as_conv1')(audio_input)
    as_conv1 = BatchNormalization()(as_conv1)
    as_conv1 = ReLU()(as_conv1)
    print('as_1:', as_conv1.shape)

    as_conv2 = Convolution2D(48, kernel_size=(7, 1), strides=(1, 1), padding='same', dilation_rate=(1, 1), name='as_conv2')(as_conv1)
    as_conv2 = BatchNormalization()(as_conv2)
    as_conv2 = ReLU()(as_conv2)
    print('as_2:', as_conv2.shape)

    as_conv3 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(1, 1), name='as_conv3')(as_conv2)
    as_conv3 = BatchNormalization()(as_conv3)
    as_conv3 = ReLU()(as_conv3)
    print('as_3:', as_conv3.shape)

    as_conv4 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(2, 1), name='as_conv4')(as_conv3)
    as_conv4 = BatchNormalization()(as_conv4)
    as_conv4 = ReLU()(as_conv4)
    print('as_4:', as_conv4.shape)

    as_conv5 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(4, 1), name='as_conv5')(as_conv4)
    as_conv5 = BatchNormalization()(as_conv5)
    as_conv5 = ReLU()(as_conv5)
    print('as_5:', as_conv5.shape)

    as_conv6 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(8, 1), name='as_conv6')(as_conv5)
    as_conv6 = BatchNormalization()(as_conv6)
    as_conv6 = ReLU()(as_conv6)
    print('as_6:', as_conv6.shape)

    as_conv7 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(16, 1), name='as_conv7')(as_conv6)
    as_conv7 = BatchNormalization()(as_conv7)
    as_conv7 = ReLU()(as_conv7)
    print('as_7:', as_conv7.shape)

    as_conv8 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(1, 1), name='as_conv8')(as_conv7)
    as_conv8 = BatchNormalization()(as_conv8)
    as_conv8 = ReLU()(as_conv8)
    print('as_8:', as_conv8.shape)

    as_conv9 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(2, 2), name='as_conv9')(as_conv8)
    as_conv9 = BatchNormalization()(as_conv9)
    as_conv9 = ReLU()(as_conv9)
    print('as_9:', as_conv9.shape)

    as_conv10 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(4, 4), name='as_conv10')(as_conv9)
    as_conv10 = BatchNormalization()(as_conv10)
    as_conv10 = ReLU()(as_conv10)
    print('as_10:', as_conv10.shape)

    as_conv11 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(8, 8), name='as_conv11')(as_conv10)
    as_conv11 = BatchNormalization()(as_conv11)
    as_conv11 = ReLU()(as_conv11)
    print('as_11:', as_conv11.shape)

    as_conv12 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(16, 16), name='as_conv12')(as_conv11)
    as_conv12 = BatchNormalization()(as_conv12)
    as_conv12 = ReLU()(as_conv12)
    print('as_12:', as_conv12.shape)

    as_conv13 = Convolution2D(8, kernel_size=(1, 1), strides=(1, 1), padding='same', dilation_rate=(1, 1), name='as_conv13')(as_conv12)
    as_conv13 = BatchNormalization()(as_conv13)
    as_conv13 = ReLU()(as_conv13)
    print('as_13:', as_conv13.shape)

    AS_out = Reshape((298, 8 * 257))(as_conv13)
    print('AS_out:', AS_out.shape)

    ### Vibrometer subnetwork
    vibro_input = Input(shape=(298, 257, 2))
    print('vs_0:', vibro_input.shape)
    vs_conv1 = Convolution2D(48, kernel_size=(1, 7), strides=(1, 1), padding='same', dilation_rate=(1, 1), name='vs_conv1')(vibro_input)
    vs_conv1 = BatchNormalization()(vs_conv1)
    vs_conv1 = ReLU()(vs_conv1)
    print('vs_1:', vs_conv1.shape)

    vs_conv2 = Convolution2D(48, kernel_size=(7, 1), strides=(1, 1), padding='same', dilation_rate=(1, 1), name='vs_conv2')(vs_conv1)
    vs_conv2 = BatchNormalization()(vs_conv2)
    vs_conv2 = ReLU()(vs_conv2)
    print('vs_2:', vs_conv2.shape)

    vs_conv3 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(1, 1), name='vs_conv3')(vs_conv2)
    vs_conv3 = BatchNormalization()(vs_conv3)
    vs_conv3 = ReLU()(vs_conv3)
    print('vs_3:', vs_conv3.shape)

    vs_conv4 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(2, 1), name='vs_conv4')(vs_conv3)
    vs_conv4 = BatchNormalization()(vs_conv4)
    vs_conv4 = ReLU()(vs_conv4)
    print('vs_4:', vs_conv4.shape)

    vs_conv5 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(4, 1), name='vs_conv5')(vs_conv4)
    vs_conv5 = BatchNormalization()(vs_conv5)
    vs_conv5 = ReLU()(vs_conv5)
    print('vs_5:', vs_conv5.shape)

    vs_conv6 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(8, 1), name='vs_conv6')(vs_conv5)
    vs_conv6 = BatchNormalization()(vs_conv6)
    vs_conv6 = ReLU()(vs_conv6)
    print('vs_6:', vs_conv6.shape)

    vs_conv7 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(16, 1), name='vs_conv7')(vs_conv6)
    vs_conv7 = BatchNormalization()(vs_conv7)
    vs_conv7 = ReLU()(vs_conv7)
    print('vs_7:', vs_conv7.shape)

    vs_conv8 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(1, 1), name='vs_conv8')(vs_conv7)
    vs_conv8 = BatchNormalization()(vs_conv8)
    vs_conv8 = ReLU()(vs_conv8)
    print('vs_8:', vs_conv8.shape)

    vs_conv9 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(2, 2), name='vs_conv9')(vs_conv8)
    vs_conv9 = BatchNormalization()(vs_conv9)
    vs_conv9 = ReLU()(vs_conv9)
    print('vs_9:', vs_conv9.shape)

    vs_conv10 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(4, 4), name='vs_conv10')(vs_conv9)
    vs_conv10 = BatchNormalization()(vs_conv10)
    vs_conv10 = ReLU()(vs_conv10)
    print('vs_10:', vs_conv10.shape)

    vs_conv11 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(8, 8), name='vs_conv11')(vs_conv10)
    vs_conv11 = BatchNormalization()(vs_conv11)
    vs_conv11 = ReLU()(vs_conv11)
    print('vs_11:', vs_conv11.shape)

    vs_conv12 = Convolution2D(48, kernel_size=(5, 5), strides=(1, 1), padding='same', dilation_rate=(16, 16), name='vs_conv12')(vs_conv11)
    vs_conv12 = BatchNormalization()(vs_conv12)
    vs_conv12 = ReLU()(vs_conv12)
    print('vs_12:', vs_conv12.shape)

    vs_conv13 = Convolution2D(8, kernel_size=(1, 1), strides=(1, 1), padding='same', dilation_rate=(1, 1), name='vs_conv13')(vs_conv12)
    vs_conv13 = BatchNormalization()(vs_conv13)
    vs_conv13 = ReLU()(vs_conv13)
    print('vs_13:', vs_conv13.shape)

    VS_out = Reshape((298, 8 * 257))(vs_conv13)
    print('AS_out:', VS_out.shape)

    ### fusion, LSTM, and FC layers
    AVfusion_list = [AS_out, VS_out]
    AVfusion = concatenate(AVfusion_list, axis=2)
    print('AVfusion:', AVfusion.shape)

    lstm = Bidirectional(LSTM(200,input_shape=(298,8*257*2),return_sequences=True),merge_mode='sum')(AVfusion)
    print('lstm:', lstm.shape)

    fc1 = Dense(300, name="fc1", activation='relu', kernel_initializer=he_normal(seed=27))(lstm)
    print('fc1:', fc1.shape)
    fc2 = Dense(300, name="fc2", activation='relu', kernel_initializer=he_normal(seed=42))(fc1)
    print('fc2:', fc2.shape)
    fc3 = Dense(300, name="fc3", activation='relu', kernel_initializer=he_normal(seed=65))(fc2)
    print('fc3:', fc3.shape)

    complex_mask = Dense(257 * 2 * 2, name="complex_mask", kernel_initializer=glorot_uniform(seed=87))(fc3)
    print('complex_mask:', complex_mask.shape)

    complex_mask_out = Reshape((298, 257, 2, 2))(complex_mask)
    print('complex_mask_out:', complex_mask_out.shape)

    model = Model(inputs=[audio_input, vibro_input], outputs=complex_mask_out)
    return model
