import matplotlib.pyplot as plt
from example_generator import *
import sys
sys.path.append('../../data')
from data_utils import GetAmp,Dataset,ReadRawData
from utils import *
import scipy.io.wavfile

dataset_base = '../../data/dataset2'
dataset = Dataset()
dataset.ReadFileIdMapping('%s/meta/file2id.txt' %(dataset_base),
                          '%s/meta/id2file.txt' %(dataset_base))

def GetFilenames(dataset, example_filename):
    idx = int(example_filename.split('/')[-1].split('-')[-1].split('.')[0].split('_')[1])
    f1_snd, f2_snd, f1_vib = dataset.DecodeKey(dataset.id2file[idx])
    return f1_snd, f2_snd

with open('/hdd1/research_data/vibsnd_dataset/training_instance/instance-1/16/saved_models/test_files.txt', 'r') as stream:
    filenames = stream.readlines()
    filenames = [x.strip('\n') for x in filenames]
    generator = VibroSoundGenerator(filenames, dataset_base, batch_size=10)
    X, y = generator.test_data_generation(filenames[:10])
    mix, vib1 = X
    crm1 = y[:,:,:,:,0]
    crm2 = y[:,:,:,:,1]
    N_examples = mix.shape[0]
    print('N_examples = ', N_examples)
    xlim = [0, 0.5]
    ylim = [0, 10000]
    vmin = -200
    vmax = 0
    for ii in range(N_examples):

        f1snd, f2snd = GetFilenames(dataset, filenames[ii][:-1])
        print(f1snd, f2snd)
        # _, data1 = scipy.io.wavfile.read('../../data/' + f1snd[:-3] + 'wav')
        # _, data2 = scipy.io.wavfile.read('../../data/' + f2snd[:-3] + 'wav')
        data1 = ReadRawData('../../data/' + f1snd)
        data2 = ReadRawData('../../data/' + f2snd)

        plt.figure(figsize=[16, 8])

        plt.subplot(2,3,1)
        # plt.plot(data2)
        plt.specgram(data1,
                     NFFT=512, Fs=48000, cmap='jet')
        plt.ylabel('Frequency (Hz)')
        plt.xlim(xlim)
        plt.ylim(ylim)
        plt.title('Sound 1')

        plt.subplot(2,3,4)
        plt.specgram(data2,
                     NFFT=512, Fs=48000, cmap='jet')
        plt.ylabel('Frequency (Hz)')
        plt.xlabel('Time (s)')
        plt.xlim(xlim)
        plt.ylim(ylim)
        plt.title('Sound 2')

        plt.subplot(2,3,2)
        plt.specgram(fast_istft(mix[ii,:,:,:]),
                     NFFT=512, Fs=48000, cmap='jet')
        plt.xlim(xlim)
        plt.ylim(ylim)
        plt.title('Sound mix')

        plt.subplot(2,3,5)
        plt.specgram(fast_istft(vib1[ii,:,:,:]),
                     NFFT=512, Fs=48000, cmap='jet')
        plt.xlabel('Time (s)')
        plt.xlim(xlim)
        plt.ylim(ylim)
        plt.title('Acceleration')

        plt.subplot(2,3,3)
        plt.specgram(fast_istft(crm1[ii,:,:,:]),
                     NFFT=512, Fs=48000, cmap='jet')
        plt.xlim(xlim)
        plt.ylim(ylim)
        plt.title('CRM 1')

        plt.subplot(2,3,6)
        plt.specgram(fast_istft(crm2[ii,:,:,:]),
                     NFFT=512, Fs=48000, cmap='jet')
        plt.xlabel('Time (s)')
        plt.xlim(xlim)
        plt.ylim(ylim)
        plt.title('CRM 2')
        # plt.pcolor(np.log(GetAmp(crm1[ii,:,:,:])))

        plt.savefig('tmp/example_%u.pdf' %(ii))

        # plt.figure()
        # plt.subplot(1,2,1)
        # plt.specgram(fast_istft(fast_icRM(mix[ii,:,:,:], crm1[ii,:,:,:])),
        #              NFFT=512, Fs=48000, cmap='jet')
        # plt.xlim(xlim)
        # plt.ylim(ylim)
        # plt.subplot(1,2,2)
        # plt.specgram(fast_istft(fast_icRM(mix[ii,:,:,:], crm2[ii,:,:,:])),
        #              NFFT=512, Fs=48000, cmap='jet')
        # plt.xlim(xlim)
        # plt.ylim(ylim)
        # plt.show()
