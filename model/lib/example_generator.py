import numpy as np
import keras,sys
sys.path.append('../../data')
from data_utils import Dataset
################################################################################
class VibroSoundGenerator(keras.utils.Sequence):
    '''
    Generates train/test/val vibration/audio data for Keras
    @params Xdim tensor of shape [#time bins, #freq bins, 2, 2], 3rd dim is 0: real, 1: imag; 4th dim is 0: mix stft; 1: vib1 stft
    @params ydim tensor of shape [#time bins, #freq bins, 2, 2], 3rd dim is 0: real, 1: imag; 4th dim is 0: crm1 stft; 1: crm2 stft
    NOTE: X.shape = [#example, *Xdim], and similarly for y
    '''
    def __init__(self, dataset_base, example_filenames,
                 Xdim=(298, 257, 2, 2),
                 ydim=(298, 257, 2, 2),
                 batch_size=4,
                 shuffle=True):
        '''
        Initialization
        '''
        self.dataset_base = dataset_base
        self.indexes = [] # overwritten by on_epoch_end()
        self.filenames = example_filenames
        self.filenames = ['%s/examples/%s' %(self.dataset_base,
                                    x.split('/')[-1]) for x in self.filenames]
        self.Xdim = Xdim
        self.ydim = ydim
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.on_epoch_end()

    def __len__(self):
        '''
        Denotes the number of batches per epoch
        '''
        return int(np.floor(len(self.filenames) / self.batch_size))

    def __getitem__(self, index):
        '''
        Generate one batch of data
        '''
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        # Find list
        filenames_temp = [self.filenames[k] for k in indexes]
        # Generate data
        X, y = self.__data_generation(filenames_temp)
        return X, y

    def on_epoch_end(self):
        '''
        Updates indexes after each epoch
        '''
        self.indexes = np.arange(len(self.filenames))
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def test_data_generation(self, filenames_temp):
        return self.__data_generation(filenames_temp)

    def __data_generation(self, filenames_temp):
        '''
        Generates data containing batch_size samples
        '''
        # Initialization
        X = np.empty((self.batch_size, *self.Xdim))
        y = np.empty((self.batch_size, *self.ydim))

        # Generate data
        for i, filename in enumerate(filenames_temp):
            X[i,], y[i,] = np.load(filename)
        return [X[:,:,:,:,0], X[:,:,:,:,1]], y
################################################################################
if __name__ == '__main__':
    with open('../../data/dataset/meta/filenames.txt', 'r') as stream:
        filenames = stream.readlines()
        filenames = [x.strip('\n') for x in filenames]
        generator = VibroSoundGenerator(filenames, '../../data/dataset')
        X, y = generator.test_data_generation(filenames[:4])
        print(X[0].shape, y.shape)
